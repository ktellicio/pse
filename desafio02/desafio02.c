#include <iostream>
#include <string.h>     /* memset   */
#include <stdlib.h>     /* random   */
#include <math.h>       /* sine     */
#include <time.h>       /* clock    */

#define PI                  3.14159265

/* To use for Taylor Series*/
#define INV_THREE_FACTORIAL 0.16666666666
#define INV_FIVE_FACTORIAL  0.00833333333
#define INV_SEVEN_FACTORIAL 0.00019841269

/* Calculating mean of sin(x) */
#define NUMBER_SAMPLES_MEAN 1000
#define ARRAY_SIZE          8
#define SAMPLES_PER_ELEMENT (NUMBER_SAMPLES_MEAN/ARRAY_SIZE)

/* Calculating the mean of improvement */
#define NUMBER_OF_SAMPLES 1000

void slowFunction() {
    int index;
    int randomInDegrees;
	float x;
    float sinX;
    
    float mean[ARRAY_SIZE];
	memset(mean, 0, sizeof(mean));
	
	for(int i = 0; i < NUMBER_SAMPLES_MEAN; i++) { 
	    randomInDegrees = rand() % 360;
	    randomInDegrees /= 4;                       /* Divide by Constant */
	    x = (float) randomInDegrees *(PI/180);
	    sinX = sin(x);                              /* Sine by math.h */
	    
	    index = i % ARRAY_SIZE;                     /* Standard Modulo */
        mean[index] += sinX / SAMPLES_PER_ELEMENT;  /* Rolling Mean */
	}
	/*
    for(int j = 0; j < ARRAY_SIZE; ++j) {
        printf("%2.3f, ", mean[j]);
    }
    printf("\n");
    */
}

void fastFunction(){
    /*  Better with:
        Bit Shifting Modulo, Mean, Taylor Serie, 
        Factor Polynomials (or Horner's scheme) and Division by Constant*/
        
    int index;
    int randomInDegrees;
	double x;
    float sinX;
    
    float xSq;
	
    float sum[ARRAY_SIZE];
    memset(sum, 0, sizeof(sum));

    for(int i = 0; i < NUMBER_SAMPLES_MEAN; i++) { 
	    randomInDegrees = rand() % 360;
	    randomInDegrees >>= 2;                       /* Divide by Constant */
	    x = (float) randomInDegrees *(PI/180);
	    
    	// Sine by Taylor Series (Horner's scheme, which applies Factor Polynomials):
        xSq = x*x;
        sinX = (float) x * (1 - xSq * (INV_THREE_FACTORIAL - xSq * (INV_FIVE_FACTORIAL - INV_SEVEN_FACTORIAL * xSq)));
 
        index = (i & ARRAY_SIZE-1);	    /* Modulo */
        sum[index] = sinX;          /* Block Mean */
    }
    
        for(int j = 0; j < ARRAY_SIZE; ++j) {
        sum[j] = sum[j] / SAMPLES_PER_ELEMENT; /* Block Mean */
    }

	/*
    for(int j = 0; j < ARRAY_SIZE; ++j) {
        printf("%2.3f, ", sum[j]);
    }
    printf("\n");
    */
}
int main() {
    
    clock_t startTime, endTime;
    
    startTime = clock();
    for(int i = 0; i < NUMBER_OF_SAMPLES; i++) {
        slowFunction();
    }
    endTime = clock();
    long int slower = endTime - startTime;
    
    startTime = clock();
    for(int i = 0; i < NUMBER_OF_SAMPLES; i++) {
        fastFunction();
    }
    endTime = clock();
    long int faster = endTime - startTime;
    float percentage = 100*((float) (slower-faster)/slower);
    printf("Faster: %ld Slower: %ld\n", faster, slower);
    printf("Porcentagem de ganho: %.2f%\n", percentage);
	return 0;
}